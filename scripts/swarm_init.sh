
echo "DOCKER SWARM INIT"

ip=$(hostname -i | grep -Po '(\d+\.\d+\.\d+\.\d+)' | head -n1)

docker swarm init \
	--advertise-addr=$ip:2377 \
	--listen-addr=0.0.0.0:2377 \
	--default-addr-pool 10.203.0.0/16 \
	--default-addr-pool 10.204.0.0/16 \
	--default-addr-pool 10.205.0.0/16 \
	--task-history-limit=2

echo "NETWORKS: ..."
echo -e "backend: "
docker network create --driver overlay --internal backend || true
echo -e "frontend: "
docker network create --driver overlay frontend || true

echo ""

